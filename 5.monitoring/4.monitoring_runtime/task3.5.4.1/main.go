package main

import (
	"context"
	"fmt"
	"golang.org/x/sync/errgroup"
	"runtime"
	"time"
)

var in = 7

const (
	upWarning   = "⚠️ Предупреждение: Количество горутин увеличилось более чем на 20%!"
	downWarning = "⚠️ Предупреждение: Количество уменьшилось более чем на 20%!"
)

// Переменные индексации горутин
var (
	indexUP   = 0.0
	indexDOWN = 0.0
)

// updateIndexes актуализация данных по индексам
func updateIndexes(prevGoroutines int) {
	indexUP = float64(prevGoroutines) * 1.2
	indexDOWN = float64(prevGoroutines) * 0.8
}

// при увеличении кол-ва горутин на 20% стек увеличивается в двое
func monitorGoroutines(prevGoroutines int) {
	if indexUP == 0 {
		updateIndexes(prevGoroutines)
	}

	if float64(prevGoroutines) > indexUP {
		updateIndexes(prevGoroutines)
		fmt.Println(upWarning)
	} else if float64(prevGoroutines) < indexDOWN {
		updateIndexes(prevGoroutines)
		fmt.Println(downWarning)
	}
	fmt.Println("Текущее количество горутин: ", prevGoroutines)
}

// В комментариях находятся части функционала для моментального прекращения работы программы при получении ошибки из горутины
func main() {

	g, _ := errgroup.WithContext(context.Background())

	// Мониторинг горутин
	go func() {
		for {
			time.Sleep(300 * time.Millisecond)
			monitorGoroutines(runtime.NumGoroutine())
		}

	}()

	//exCh := make(chan error, 1)

	// Имитация активной работы приложения с созданием горутин
	for i := 0; i < 64; i++ {
		forI := i
		g.Go(func() error {
			time.Sleep(5 * time.Second)
			if forI == 40 {
				/*select {
				case exCh <- fmt.Errorf("Ситуация УПС в %d грутине.", forI):
				default:
					return nil
				}

				*/
				return fmt.Errorf("Ситуация УПС в %d грутине.", forI) // в этом случае программа отработает все горутины и выведет в консоль ошибку
			}

			return nil
		})
		time.Sleep(80 * time.Millisecond)
	}
	/*
		go func() {
			for {
				select {
				case err := <-exCh:
					if err != nil {
						fmt.Println(err)
						os.Exit(2)
					}
				}
			}
		}()
	*/

	// Ожидание завершения всех горутин
	if err := g.Wait(); err != nil {
		fmt.Println("Ошибка:", err)
	}
}
